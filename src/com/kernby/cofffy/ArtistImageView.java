package com.kernby.cofffy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ArtistImageView extends ImageView {
	Paint fillPaint;
	Paint strokePaint;
	Bitmap fillBitmap;
	BitmapShader fillBitmapShader;
	Matrix matrix = new Matrix();

	public ArtistImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// fillPaint = new Paint();
		// strokePaint = new Paint();
		//
		// strokePaint.setDither(true);
		// strokePaint.setColor(0xFFFFFF00);
		// strokePaint.setStyle(Paint.Style.STROKE);
		// strokePaint.setAntiAlias(true);
		// strokePaint.setStrokeWidth(3);

		fillBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.artist);
		fillBitmapShader = new BitmapShader(fillBitmap, Shader.TileMode.REPEAT,Shader.TileMode.REPEAT);

		// fillPaint.setColor(0xFFFFFFFF);
		// fillPaint.setStyle(Paint.Style.FILL);
		// fillPaint.setShader(fillBitmapShader);
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// canvas.drawCircle(240, 400, 150, fillPaint);
		// canvas.drawCircle(240, 400, 150, strokePaint);
		// canvas.save();
	}
}
