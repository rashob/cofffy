package com.kernby.cofffy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.media.AudioManager;

public class IntentReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action.equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
			context.sendBroadcast(new Intent(PlayerActivity.ACTION_BECOMING_NOISY));
		}
		else if (action.equals(MediaPlayerService.ACTION_CONTINUE)){
			context.startService(new Intent(MediaPlayerService.ACTION_CONTINUE));
		}else if (action.equals(MediaPlayerService.ACTION_NEXT)){
			context.startService(new Intent(MediaPlayerService.ACTION_NEXT));
		}else if (action.equals(MediaPlayerService.ACTION_PAUSE)){
			context.startService(new Intent(MediaPlayerService.ACTION_PAUSE));
		}else if (action.equals(MediaPlayerService.ACTION_PLAY)){
			context.startService(new Intent(MediaPlayerService.ACTION_PLAY));
		}else if (action.equals(MediaPlayerService.ACTION_PREVIOUS)){
			context.startService(new Intent(MediaPlayerService.ACTION_PREVIOUS));
		}else if (action.equals(MediaPlayerService.ACTION_START)){
			context.startService(new Intent(MediaPlayerService.ACTION_START));
		}
		
	}

}
