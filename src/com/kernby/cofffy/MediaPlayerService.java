package com.kernby.cofffy;

import java.io.IOException;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

public class MediaPlayerService extends Service implements OnCompletionListener, OnPreparedListener, OnErrorListener{
	private static final String TAG ="MediaPlayerService";
	public static final String ACTION_CONTINUE = "com.kernby.cofffy.action.CONTINUE";
	public static final String ACTION_PLAY = "com.kernby.cofffy.action.PLAY";
	public static final String ACTION_PAUSE = "com.kernby.cofffy.action.PAUSE";
	public static final String ACTION_START = "com.kernby.cofffy.action.START";
	public static final String ACTION_NEXT = "com.kernby.cofffy.action.NEXT";
	public static final String ACTION_PREVIOUS = "com.kernby.cofffy.action.PREVIOUS";
	
	final int NOTIFICATION_ID = 1;
	MediaPlayer mediaPlayer = null;
	WifiLock wifiLock;
	Notification notification;
	NotificationManager notificationManager;
	PendingIntent pendingIntent;
		
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
		
	@Override
	public void onCreate() {
		wifiLock = ((WifiManager)getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, "cofffy.wifilock");
		notificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
		super.onCreate();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		String action = intent.getAction();
		if( ACTION_START.equals(action)){
			Log.d(TAG, action);
			if (mediaPlayer != null) {
				mediaPlayer.release();
				mediaPlayer = null;
			}
			String streamUrl = intent.getStringExtra("track.streamUrl");
			
			mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(streamUrl));
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.setOnErrorListener(this);
			
			try {
				mediaPlayer.prepareAsync();
			} catch (IllegalArgumentException e) {
				Log.e(TAG, e.toString());
			} catch (IllegalStateException e) {
				Log.e(TAG, e.toString());
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			setUpAsForeground();
		}else if (ACTION_PLAY.equals(action) || ACTION_CONTINUE.equals(action)) {
			Log.d(TAG, action);
			mediaPlayer.start();
			Helper.isPlaying = true;
			updateNotification();
		}else if (ACTION_PAUSE.equals(action)) {
			Log.d(TAG, action);
			mediaPlayer.pause();
			Helper.isPlaying = false;
			relaxResources(false);
		}else if (ACTION_NEXT.equals(action) || ACTION_PREVIOUS.equals(action)) {
			Log.d(TAG, action);
			try {
				mediaPlayer.stop();
				mediaPlayer.reset();
				String streamUrl = intent.getStringExtra("track.streamUrl");
				mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(streamUrl));
				mediaPlayer.prepareAsync();
			} catch (IllegalArgumentException e) {
				Log.e(TAG, e.toString());
			} catch (SecurityException e) {
				Log.e(TAG, e.toString());
			} catch (IllegalStateException e) {
				Log.e(TAG, e.toString());
			} catch (IOException e) {
				Log.e(TAG, e.toString());
			}
			updateNotification();
		}
		Log.d(TAG,"onStartCommand");
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		relaxResources(true);
		Log.d(TAG,"onDestroy");
		super.onDestroy();
	}
	
	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		Log.e(TAG, "onError what: " + Integer.toString(what) + " - "
				+ "extra: " + Integer.toString(extra));
		return true;
	}

	@Override
	public void onPrepared(MediaPlayer arg0) {
		Log.d(TAG, "onPrepared");
		if (Helper.isPlaying) {
			mediaPlayer.start();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		Log.d(TAG,"onCompletion");
		Intent intent = new Intent(PlayerActivity.ACTION_COMPLETED);
		sendBroadcast(intent);
	}
	
	private void relaxResources(boolean releaseMediaPlayer){
		if (releaseMediaPlayer && mediaPlayer != null) {
			mediaPlayer.reset();
			mediaPlayer.release();
			mediaPlayer = null;
		}
		stopForeground(true);
		if (wifiLock.isHeld()) {
			wifiLock.release();
		}
		
	}
	
	private void setUpAsForeground() {
		Intent mIntent = new Intent(getApplicationContext(),PlayerActivity.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		mIntent.setAction(PlayerActivity.ACTION_NOTIFICATION);
		
		pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Track track = Helper.trackList.get(Helper.activeTrackListIndex);
		
		notification = new Notification();
		notification.icon = R.drawable.ic_stat_notification;
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.tickerText = track.getName();
		notification.setLatestEventInfo(getApplicationContext(), track.getName(), track.getArtistName(), pendingIntent);
		
		wifiLock.acquire();
		startForeground(NOTIFICATION_ID, notification);
    }
	
	private void updateNotification() {
		Intent mIntent = new Intent(getApplicationContext(),PlayerActivity.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		mIntent.setAction(PlayerActivity.ACTION_NOTIFICATION);
		
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Track track = Helper.trackList.get(Helper.activeTrackListIndex);
		notification.tickerText = track.getName();
		notification.setLatestEventInfo(getApplicationContext(), track.getName(), track.getArtistName(), pendingIntent);
		
		notificationManager.notify(NOTIFICATION_ID,notification);
    }
}
