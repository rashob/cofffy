package com.kernby.cofffy;

import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PlayerActivity extends Activity {
	private static final String TAG = PlayerActivity.class.getName();
	public static final String ACTION_START = "com.kernby.cofffy.action.START";
	public static final String ACTION_CONTINUE = "com.kernby.cofffy.action.CONTINUE";
	public static final String ACTION_BECOMING_NOISY = "com.kernby.cofffy.action.BECOMING_NOISY";
	public static final String ACTION_COMPLETED = "com.kernby.cofffy.action.COMPLETED";
	public static final String ACTION_NOTIFICATION = "com.kernby.cofffy.action.notification";
	public static final String COFFEE_TYPE = "coffeeType";
	Button btnHome;
	Button btnShare;
	Button btnFavourite;
	Button btnPrevious;
	Button btnPlayerAction;
	Button btnNext;
	TextView txtTrackTitle;
	TextView  txtArtistName;
	TextView  txtTrackName;
	TextView  txtArtistDesc;
	ImageView ivArtistImage;
	RelativeLayout rlTrackInfo;
	Intent actionIntent;
	Track track;
	ProgressDialog progressDialog;
	Context context;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);
		context = PlayerActivity.this;
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		btnHome = (Button) findViewById(R.id.btnHome);
		btnShare = (Button) findViewById(R.id.btnShare);
		btnFavourite = (Button) findViewById(R.id.btnFavourite);
		btnPrevious = (Button) findViewById(R.id.btnPrevious);
		btnPlayerAction = (Button) findViewById(R.id.btnPlayerAction);
		btnNext = (Button) findViewById(R.id.btnNext);
		txtTrackTitle = (TextView) findViewById(R.id.txtTrackTitle);
		txtArtistName = (TextView) findViewById(R.id.txtArtistName);
		txtTrackName = (TextView) findViewById(R.id.txtTrackName);
		txtArtistDesc = (TextView) findViewById(R.id.txtArtistDesc);
		ivArtistImage = (ImageView) findViewById(R.id.ivArtistImage);
		rlTrackInfo = (RelativeLayout) findViewById(R.id.rlTrackInfo);
		rlTrackInfo.setBackgroundResource(Helper.getRandomBackground());
		
		IntentFilter actionFilter = new IntentFilter();
		actionFilter.addAction(ACTION_COMPLETED);
		actionFilter.addAction(ACTION_BECOMING_NOISY);
		registerReceiver(mIntentReceiver, actionFilter);
		startService();
		
		String action = getIntent().getAction().toString();
		Log.d(TAG,action);
				
		if (ACTION_START.equals(action)) {
			new findTrack().execute(Helper.coffeeType,MediaPlayerService.ACTION_START);
			btnPlayerAction.setBackgroundResource(R.drawable.pause);
			Helper.isPlaying = true;
		}else if (ACTION_CONTINUE.equals(action) || ACTION_NOTIFICATION.equals(action)) {
			setActiveTrack(Helper.activeTrackListIndex);
			if (Helper.isPlaying) {
				btnPlayerAction.setBackgroundResource(R.drawable.pause);
			}else {
				btnPlayerAction.setBackgroundResource(R.drawable.play);
			}
		}
		
		btnHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		
		btnShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//TODO Intent shareIntent = new Intent(Intent.ACTION_SEND);
			}
		});
		
		btnFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Coming soon", Toast.LENGTH_SHORT).show();
			}
		});
		
		btnPrevious.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Helper.activeTrackListIndex = Math.max(0, Helper.activeTrackListIndex-1);
				track = Helper.trackList.get(Helper.activeTrackListIndex);
				Log.d(TAG,Integer.toString(Helper.activeTrackListIndex));
				setTrackViews();
				sendActionToService(MediaPlayerService.ACTION_PREVIOUS,track.getStreamUrl());		
			}
		});
		
		btnNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				btnNextClick();
			}
		});
		
		btnPlayerAction.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (Helper.isPlaying) {
					sendActionToService(MediaPlayerService.ACTION_PAUSE);
					btnPlayerAction.setBackgroundResource(R.drawable.play);
				}else {
					btnPlayerAction.setBackgroundResource(R.drawable.pause);
					sendActionToService(MediaPlayerService.ACTION_CONTINUE);
				}
				Helper.isPlaying = !Helper.isPlaying;
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		unregisterReceiver(mIntentReceiver);
		super.onDestroy();
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
	
	private void btnNextClick(){
		if (Helper.activeTrackListIndex == Helper.trackList.size()-1) {
			new findTrack().execute(Helper.coffeeType,MediaPlayerService.ACTION_NEXT);
		}else {
			if (Helper.activeTrackListIndex >= 0) {
				Helper.activeTrackListIndex = Math.min(Helper.trackList.size()-1, Helper.activeTrackListIndex+1);
			}
			track = Helper.trackList.get(Helper.activeTrackListIndex);
			setTrackViews();
			sendActionToService(MediaPlayerService.ACTION_NEXT,track.getStreamUrl());
		}
	}
		
	private void setActiveTrack(int index){
		track = Helper.trackList.get(index);
		setTrackViews();
	}
	
	private void setTrackViews(){
		txtTrackTitle.setText(track.getName());
		txtTrackName.setText(track.getName());
		txtArtistName.setText(track.getArtistName());
		txtArtistDesc.setText(track.getArtistDesc());
		
		Bitmap bitmapArtist = null;
		if (track.getImagePath() == null) {
			try {
				bitmapArtist = BitmapFactory.decodeStream(getAssets().open("dummy_artist.png"));
			} catch (IOException e) {
				Log.e(TAG, e.toString());
			}
		}else {
			bitmapArtist = BitmapFactory.decodeFile(track.getImagePath());
		}
				
		BitmapDrawable bitmapDrawableBg = (BitmapDrawable) getResources().getDrawable(R.drawable.artist_bg_b);
		Bitmap bitmapBg = bitmapDrawableBg.getBitmap();
			
		Bitmap maskBitmap = Bitmap.createBitmap(bitmapArtist.getWidth(),bitmapArtist.getHeight(), Bitmap.Config.ARGB_8888);
		Bitmap scaledBg = Bitmap.createScaledBitmap(bitmapBg, bitmapArtist.getWidth(), bitmapArtist.getHeight(), true);
		Canvas canvas = new Canvas(maskBitmap);
		Paint paint = new Paint();
		paint.setFilterBitmap(false);
		paint.setAntiAlias(true);
		canvas.drawBitmap(bitmapArtist, 0,0, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));		
		canvas.drawBitmap(scaledBg, new Matrix(), paint);
		ivArtistImage.setImageBitmap(maskBitmap);
	}
	
	private void sendActionToService(String action){
		actionIntent = new Intent(action);
		startService(actionIntent);
	}
	
	private void sendActionToService(String action, String trackStreamUrl){
		actionIntent = new Intent(action);
		actionIntent.putExtra("track.streamUrl", trackStreamUrl);
		startService(actionIntent);
	}
	
	private void startService(){
		Intent serviceIntent = new Intent(this.getApplicationContext(),MediaPlayerService.class);
		startService(serviceIntent);
	}
		
	class findTrack extends AsyncTask<String, Void, Void>{
		String action;
		@Override
		protected void onPreExecute() {
			if (Helper.isInternetAvailable(getApplicationContext())) {
				progressDialog = ProgressDialog.show(context, "", "Please wait..", true);
			}else {
				onDestroy();
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... params) {
			EightTracksWrapper eightTracksWrapper = new EightTracksWrapper(context);
			action = params[1];
			track = eightTracksWrapper.findTrack(params[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			//TEMP
//			track.setStreamUrl("mnt/sdcard/tracks/sample.mp4");
					
			Helper.trackList.add(track);
			setTrackViews();
			if (MediaPlayerService.ACTION_START.equals(action)) {
				Helper.activeTrackListIndex = 0;
				sendActionToService(MediaPlayerService.ACTION_START,track.getStreamUrl());
			}else if (MediaPlayerService.ACTION_NEXT.equals(action)) {
				Helper.activeTrackListIndex += 1;
				sendActionToService(MediaPlayerService.ACTION_NEXT,track.getStreamUrl());
			}
			try {
				progressDialog.dismiss();
			} catch (Exception e) {
				Log.v(TAG,"progressDialog.dismiss() HATASI");
			}
			super.onPostExecute(result);
		}
		
	}
	
private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_COMPLETED)) {
				btnNextClick();
			}else if (action.equals(ACTION_BECOMING_NOISY)) {
				Helper.isPlaying = false;
				btnPlayerAction.setBackgroundResource(R.drawable.play);
				sendActionToService(MediaPlayerService.ACTION_PAUSE);
			}
		}
	};
}
