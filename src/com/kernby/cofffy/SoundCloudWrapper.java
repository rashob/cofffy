package com.kernby.cofffy;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SoundCloudWrapper implements Wrapper {
	//http://api.soundcloud.com/tracks/?client_id=9554130ae29c173e32dc9662af85a192&genres=twee+rock&format=json
	private static final String TAG = SoundCloudWrapper.class.getSimpleName();
	private static final String API_KEY = "9554130ae29c173e32dc9662af85a192";
	
	@Override
	public Track findTrack(String genres) {
		Track track = new Track();
		
		String url = "http://api.soundcloud.com/tracks/?format=json&client_id="+API_KEY+"&streamable=true&limit=200&genres=" + genres;
		//TODO: Her istekte rastgele şarkı seçmek için yeni link üreten fonksiyon
		JSONArray response = null;
		try {
			response = new Helper.getJson().execute(url).get();
			if (response.get(0) == null) {
				//response = Helper.getJson(url);
				Log.d(TAG,"Sonuç yok");
			}
		} catch (JSONException e) {
			Log.e(TAG,e.toString());
		} catch (InterruptedException e) {
			Log.e(TAG,e.toString());
		} catch (ExecutionException e) {
			Log.e(TAG,e.toString());
		}
		
		Integer id = Calendar.getInstance().get(Calendar.SECOND);
		try {
			JSONObject trackObject = (JSONObject) response.get(id%10);
			track.setArtistDesc(trackObject.getString("description"));
			track.setArtistName("");
			track.setDuration((Integer)trackObject.get("duration"));
			track.setImageUrl(trackObject.getString("artwork_url"));
			if (track.getImageUrl().equals("") || track.getImageUrl() == "") {
				track.setImageUrl(trackObject.getJSONObject("user").getString("avatar_url"));
			}
			track.setName(trackObject.getString("title"));
			track.setPermalink(trackObject.getString("permalink_url"));
			track.setStreamUrl(trackObject.getString("stream_url"));
		} catch (JSONException e) {
			Log.e(TAG,e.toString());
		}
		
		track.setStreamUrl(Helper.getStreamUrl(track.getStreamUrl() + "/?client_id=" + API_KEY));
		return track;
	}
	
	@Override
	public String generateUrl(String coffeType) {
		return null;
	}

}
