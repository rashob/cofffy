package com.kernby.cofffy;

public interface Wrapper {
	public Track findTrack(String coffeType);
	public String generateUrl(String coffeType);
}
